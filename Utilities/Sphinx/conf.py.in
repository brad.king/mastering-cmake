# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

import sys

sys.path.insert(0, r'@conf_path@')

source_suffix = '.rst'
master_doc = 'index'

project = 'Mastering CMake'
copyright = '@conf_copyright@'
version = '@conf_version@' # feature version
release = '@conf_release@' # full version string
pygments_style = 'colors.CMakeTemplateStyle'

primary_domain = 'cmake'
highlight_language = 'none'

exclude_patterns = [
    'dev', # ignore developer-only documentation
    ]

exclude_patterns = [
    'cmake/*.rst',
    'cmake/Help/*.rst',
    'cmake/Help/command',
    'cmake/Help/cpack_*',
    'cmake/Help/dev',
    'cmake/Help/envvar',
    'cmake/Help/generator',
    'cmake/Help/manual',
    'cmake/Help/module',
    'cmake/Help/policy',
    'cmake/Help/prop_*',
    'cmake/Help/release',
    'cmake/Help/variable',
    'cmake/Licenses',
    'cmake/Source**',
    'cmake/Tests**',
    'cmake/Utilities**'
    ]
extensions = ['cmake', 'numfig', 'sphinx.ext.intersphinx']
templates_path = ['@conf_path@/templates']

nitpicky = True
intersphinx_mapping = {'cmake': ('http://www.cmake.org/cmake/help/latest', None)}

latex_show_urls='no'

html_show_sourcelink = True
html_static_path = ['@conf_path@/static']
html_style = 'cmake.css'
html_theme = 'default'
html_theme_options = {
    'footerbgcolor':    '#00182d',
    'footertextcolor':  '#ffffff',
    'sidebarbgcolor':   '#e4ece8',
    'sidebarbtncolor':  '#00a94f',
    'sidebartextcolor': '#333333',
    'sidebarlinkcolor': '#00a94f',
    'relbarbgcolor':    '#00529b',
    'relbartextcolor':  '#ffffff',
    'relbarlinkcolor':  '#ffffff',
    'bgcolor':          '#ffffff',
    'textcolor':        '#444444',
    'headbgcolor':      '#f2f2f2',
    'headtextcolor':    '#003564',
    'headlinkcolor':    '#3d8ff2',
    'linkcolor':        '#2b63a8',
    'visitedlinkcolor': '#2b63a8',
    'codebgcolor':      '#eeeeee',
    'codetextcolor':    '#333333',
}
html_title = 'Mastering CMake'
html_short_title = 'Mastering CMake'
html_favicon = '@conf_path@/static/cmake-favicon.ico'
