.. title:: Mastering CMake

Introduction
############

This book describes how to use the CMake family of tools, including CTest,
CPack and CDash, to develop, build, test, and package software for
distribution. Starting with the basics of how to install and run the
command-line and GUI tools, Mastering CMake covers how to write CMake
code as well as how to convert existing projects to CMake. Finally, a series
of guides, including the CMake tutorial, provide hands-on examples.

Mastering CMake
###############

.. toctree::
   :maxdepth: 1

   chapter/Why CMake
   chapter/Getting Started
   chapter/Writing CMakeLists Files
   chapter/CMake Cache
   chapter/Key Concepts
   chapter/Policies
   chapter/Modules
   chapter/Install
   chapter/System Inspection
   chapter/Finding Packages
   chapter/Custom Commands
   chapter/Converting Existing Systems To CMake
   chapter/Cross Compiling With CMake
   chapter/Packaging With CPack
   chapter/Testing With CMake and CTest
   chapter/CDash
   cmake/Help/guide/tutorial/index
   cmake/Help/guide/user-interaction/index
   cmake/Help/guide/using-dependencies/index
   cmake/Help/guide/importing-exporting/index
   cmake/Help/guide/ide-integration/index

.. only:: html

 Index and Search
 ################

 * :ref:`genindex`
 * :ref:`search`
